<script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="/api/plugin-file/register-form/data.js?v=1.3.0"></script>
<script src="/js/handlers.js?v=1.0.0"></script>

<h3 class="text-center">Înscrie-te pentru a alerga 5 km</h3>
<h6 class="text-center">Susține proiectul „Alerg Pentru O Cauză”</h6>
<hr>
<p class="text-center">După completarea formularului vei primi un email de confirmare.</p>

<form class="form-horizontal" method="POST" action="/new-user" enctype="multipart/form-data">
    <fieldset>

    <input name="formId" type="hidden" value="register-form">
    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="name">Nume și prenume</label>
      <div class="col-md-6">
      <input id="name" name="name" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block"></span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">Adresă de email</label>
      <div class="col-md-6">
      <input id="email" name="email" type="email" placeholder="" class="form-control input-md" required="">
      <span class="help-block">Adresa ta de email. Va fi folosită pentru confirmarea înscrierii. De asemenea va fi publică.</span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="phone">Telefon</label>
      <div class="col-md-6">
      <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block">Numărul tău de telefon.</span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">Oraș</label>
      <div class="col-md-6">
      <input id="city" name="city" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block"></span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="country">Țară</label>
      <div class="col-md-6">
      <input id="country" name="country" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block"></span>
      </div>
    </div>

    <!-- Select Basic -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="segment">Tură</label>
      <div class="col-md-6">
        <select id="segment" name="segment" class="form-control">
        </select>
        <span class="help-block">Alege-ți un segment din traseu în care să alergi.<br>
        Vom avea cu noi o masina de susținere care ne va insoți. În măsura posibilităților, te putem lua/lăsa dintr-un punct anume.</span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="galantom-title">Titlul paginii tale pe Galantom</label>
      <div class="col-md-6">
      <input id="galantom-title" name="galantom-title" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block">Vei avea o pagină personalizată pe <a href="http://alergotura.galantom.ro/">Galantom.ro</a> unde oamenii vor putea face donații cu cardul.</span>
      </div>
    </div>

    <!-- Textarea -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="message">Mesaj</label>
      <div class="col-md-6">
        <textarea required="" class="form-control" id="message" name="message" placeholder="Care este povestea ta și încurajearea ca oamenii să doneze pentru această cauză?"></textarea>
        <span class="help-block"></span>
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="goal">Suma</label>
      <div class="col-md-6">
      <input id="goal" name="goal" type="text" placeholder="" class="form-control input-md" required="">
      <span class="help-block">Suma țintă pe care vrei să o strângi împreună cu susținătorii tăi (în lei). Daca vrei doar sa alergi cu noi, ești binevenit, trece <code>0</code> în căsuță.</span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="avatar">Poză</label>
      <div class="col-md-6">
        <input type="hidden" id="avatar" value="">
        <img src="http://www.gravatar.com/avatar/c4561b0b48e1781aa36eddb864882602" class="avatar-img hide" width="500">
        <span class="help-block">
            <button type="button" class="btn btn-info btn-img-upload">Încarcă o altă imagine</button>
            <input type="file" name="avatar" class="file-upload hide">
        </span>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-4 control-label" for="confirm"></label>
      <div class="col-md-6">
        <label class="checkbox-inline" for="confirm-0">
          <input type="checkbox" name="confirm" id="confirm-0" value="1">
          Sund de acord ca datele introduse sa fie procesate conform <a href="http://galantom.ro/tos/" target="blank">teremenilor si conditiilor site-ului galantom.ro</a>.
        </label>
      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="submit-form"></label>
      <div class="col-md-6">
        <button id="submit-form" name="submit-form" class="btn btn-block btn-primary">Trimite</button>
      </div>
    </div>

    </fieldset>
</form>
