<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="/js/handlers.js"></script>

<h1 class="text-center">Mulțumim!</h1>
<p class="text-center">Mulțumim pentru înscriere. Vei primi prin email un link pentru confirmarea înscrierii. Te rugăm să-ți verifici și folderul de Spam/Junk.</p>
