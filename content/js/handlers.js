$(document).on("ready", function () {
    $("form").on("submit", function () {
        var avatar = $(".avatar-img").clone().attr("class", "")[0];
        var confirmed = $("[type='checkbox']");
        if (!confirmed.is(":checked")) {
            alert("Trebuie să fii de acord cu termenii Galantom.ro");
            return false;
        }
    });
    if (location.search === "?embed=true") {
        $(".footer").hide();
        $("body").css("background", "transparent");
    }
    $(".btn-img-upload").on("click", function () {
        $(this).next().click();
    });
    $(".file-upload").on("change", function () {
        if (!$(this).val()) { return; }
        if (!this.files || !this.files[0]) { return; }
        var reader = new FileReader();

        // image loaded
        reader.onload = function(event) {
            var dataUri = event.target.result;
            var $img = $("<img>", {
                src: dataUri
            }).on("load", function () {
                $("#avatar").val(dataUri);
                $(".avatar-img").attr("src", dataUri).removeClass("hide");
            });
       };

        // error
       reader.onerror = function(event) {
           alert("Imaginea nu a putut fi încărcată: " + event.target.error.code);
       };

       reader.readAsDataURL(this.files[0]);
    });

    var $segment = $("#segment");
    var $options = [];
    for (var i = 0; i < DATA.length; ++i) {
        $options.push(
            $("<option>", {
                value: i
              , text: "Tura " + (i + 1) + ": Start: km " + DATA[i].start + " " +  DATA[i].location + ", Data: " + DATA[i].date + ", Ora " + DATA[i].hour
            })
        )
    }
    $segment.append($options);
});
